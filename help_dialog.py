import os, gi, csv, gettext
gi.require_version('Gtk', '3.0')

version =  "1.3.1"
release_date = "26/10/2021"

t = gettext.translation(domain='messages', localedir='locale', fallback=True)
_ = t.gettext

from gi.repository import Gtk, Gdk
script_dir = os.path.dirname(os.path.realpath(__file__))

class HelpDialog(Gtk.Dialog):
    def __init__(self, parent):
        dialog = Gtk.Dialog.__init__(self, 'Aide - TableGimper', parent=parent, modal=True)
        self.set_border_width(10)
        self.set_default_size(1800,1200)

        notebook = Gtk.Notebook.new()

        page1 = Gtk.ScrolledWindow.new()
        page1_vp = Gtk.Viewport.new()
        tab1_lbl = Gtk.Label.new(_('Generate tables'))
        pg1_box = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing = 20)
        pg1_lbl1 = Gtk.Label.new("")
        pg1_lbl1.set_markup(_("<b>This action is the first step necessary to any treatment.</b>\n\n"
                            "It searches for .xcf files from the root folder, with the given search depths.\n"
                            "It then generates 2 identical tables:\n"
                            "   <b>• xcf_new_table.csv</b>, which will later represent the changes you want to make\n"
                            "   <b>• xcf_orig_table.csv</b>, which represents the original state of your xcfs.\n"
                            "     You may not edit this file unless you want to discard entire lines.\n"
                            "     It is only used by tableGimper as a safety check to ensure the changes you made to new_table.csv\n"
                            "     are based on the current contents of the xcfs.\n\n"
                            "If no .xcf was found during this action, empty files will be created. You can use them later to create new xcfs.\n\n"
                            "The 👁 and ◡ symbols indicate the visibility of a layer.\n"
                            "The column separator is the tabulation.\n\n"
                            "<b>An example of a generated table (the header line is not included in the csv files):</b>"))
        pg1_lbl1.set_halign(Gtk.Align.START)
        pg1_lbl1.set_margin_start(20)
        pg1_lbl1.set_selectable(True)
        pg1_box.pack_start(pg1_lbl1, expand=False, fill=True, padding=20)
        pg1_table_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        pg1_table_box.set_margin_start(20)
        pg1_table_box.set_halign(Gtk.Align.START)
        pg1_table = Gtk.Table(n_rows=1, n_columns=1, homogeneous=False)
        self.make_table('ex_gen_table.csv', pg1_table_box, pg1_table)
        pg1_table_box.pack_start(pg1_table, expand=True, fill=True, padding=0)
        pg1_box.pack_start(pg1_table_box, expand=False, fill=True, padding=0)
        page1_vp.add(pg1_box)
        page1.add(page1_vp)
        notebook.append_page(page1, tab1_lbl)

        page2 = Gtk.ScrolledWindow.new()
        page2_vp = Gtk.Viewport.new()
        tab2_lbl = Gtk.Label.new(_('Modify layers and masks'))
        pg2_box = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing = 20)
        pg2_lbl1 = Gtk.Label.new("")
        pg2_lbl1.set_markup(_("<b>In this treatment mode, you can use a table to create new xcfs and/or modify the contents of existing xcfs.</b>\n\n"
                            "Steps to follow :\n"
                            "   1. Generate the tables with the dedicated action\n"
                            "   2. If some lines don't require any modification, remove them from both tables\n"
                            "   3. Edit new_table.csv according to your desired modifications.\n\n"
                            "The following example shows the formatting of all possible treatments (the colors are just there for explanation):\n"
                            "   • <span foreground=\"#1698E9\"><b>Blue</b></span> : Image files to import as layers. <b>Note:</b> they must be in the same folder as the xcfs they are imported into.\n"
                            "   • <span foreground=\"#25DC9A\"><b>Teal</b></span> : Non-existing xcfs to be created\n"
                            "   • <span background=\"#ffccf7\" foreground=\"black\"> Pink background </span> : Erased layer to be deleted (here B1)\n"
                            "   • <span foreground=\"#fc5c00\"><b>{</b></span>In curly brackets<span foreground=\"#fc5c00\"><b>}</b></span> : Add a copy of another layer's mask."))
        pg2_lbl1.set_halign(Gtk.Align.START)
        pg2_lbl1.set_margin_start(20)
        pg2_lbl1.set_selectable(True)
        pg2_box.pack_start(pg2_lbl1, expand=False, fill=True, padding=20)

        pg2_table_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        pg2_table_box.set_margin_start(50)
        pg2_table_box.set_halign(Gtk.Align.START)
        pg2_table = Gtk.Table(n_rows=1, n_columns=1, homogeneous=False)
        self.make_table('ex_modif_table.csv', pg2_table_box, pg2_table)
        pg2_box.pack_start(pg2_lbl1, expand=False, fill=True, padding=20)
        pg2_table_box.pack_start(pg2_table, expand=True, fill=True, padding=0)
        pg2_box.pack_start(pg2_table_box, expand=False, fill=True, padding=0)

        pg2_lbl2 = Gtk.Label.new("")
        pg2_lbl2.set_markup(_("<b>Notes :</b>\n" +
                                "   ▪ As you can see, <span foreground=\"#1698E9\"><b>B2.png</b></span> can take <span foreground=\"#fc5c00\"><b>{</b></span>B1<span foreground=\"#fc5c00\"><b>}</b></span>'s mask despite B1 being <span background=\"#ffccf7\" foreground=\"black\"> erased </span>.\n"
                                "   ▪ If you import a file in place of an existing layer:\n"
                                "           • The existing mask will stay in place (see figure below)\n"
                                "           • Therefore, you don't need to add its name <span foreground=\"#fc5c00\"><b>{</b></span>in curly brackets<span foreground=\"#fc5c00\"><b>}</b></span>\n"
                                "           • You can however forcefully use another layer's mask with the brackets\n"
                                "           • If you don't want to use any mask:\n"
                                "                   ◦ Instead of replacing the layer, empty its cell and insert the new image\n"
                                "                       in an available space to the right of the table.\n"
                                "   ▪ A layer can be replaced by a file only if the file name differs from all the existing layers.\n"
                                "   ▪ You can switch a layer's visibility by changing the 👁 or ◡ symbol.\n\n"
                                "<b>All the possible layer movements :</b>"))
        pg2_lbl2.set_halign(Gtk.Align.START)
        pg2_lbl2.set_margin_start(20)
        pg2_lbl2.set_selectable(True)
        pg2_box.pack_start(pg2_lbl2, expand=False, fill=True, padding=20)
        movements_image = Gtk.Image.new_from_file(os.path.join(script_dir,_("images/movements.png")))
        movements_image.set_margin_start(50)
        movements_image.set_margin_bottom(50)
        movements_image.set_halign(Gtk.Align.START)
        pg2_box.pack_start(movements_image, expand=False, fill=True, padding=0)

        pg2_lbl3 = Gtk.Label.new("")
        pg2_lbl3.set_markup(_("<b>Note:</b> The 'Add mask' checkbox in tableGimper not only adds the extracted channel as a mask but also as a layer.\n"
                                "           This is for convenience, to prepare your manual masking workflow a little more."))
        pg2_lbl3.set_halign(Gtk.Align.START)
        pg2_lbl3.set_margin_start(20)
        pg2_lbl3.set_selectable(True)
        pg2_box.pack_start(pg2_lbl3, expand=False, fill=True, padding=20)

        page2_vp.add(pg2_box)
        page2.add(page2_vp)
        notebook.append_page(page2, tab2_lbl)

        page3 = Gtk.ScrolledWindow.new()
        page3_vp = Gtk.Viewport.new()
        tab3_lbl = Gtk.Label.new(_('Rename layers'))
        pg3_box = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing = 20)
        pg3_lbl1 = Gtk.Label.new("")
        pg3_lbl1.set_markup(_("<b>This mode allows you to rename layers as well as change their visibility.</b>\n\n"
                            "Once the tables are generated, change the layer names in new_table.csv and run this treatment.\n"))
        pg3_lbl1.set_halign(Gtk.Align.START)
        pg3_lbl1.set_margin_start(20)
        pg3_lbl1.set_selectable(True)
        pg3_box.pack_start(pg3_lbl1, expand=False, fill=True, padding=20)
        page3_vp.add(pg3_box)
        page3.add(page3_vp)
        notebook.append_page(page3, tab3_lbl)

        page4 = Gtk.ScrolledWindow.new()
        page4_vp = Gtk.Viewport.new()
        tab4_lbl = Gtk.Label.new(_('Export to jpg'))
        pg4_box = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing = 20)
        pg4_lbl1 = Gtk.Label.new("")
        pg4_lbl1.set_markup(_("<b>This mode allows you to export xcfs contained in new_table.csv as jpg files.</b>\n\n"
                            "orig_table is ignored and only the first column of new_table is used in this mode."))
        pg4_lbl1.set_halign(Gtk.Align.START)
        pg4_lbl1.set_margin_start(20)
        pg4_lbl1.set_selectable(True)
        pg4_box.pack_start(pg4_lbl1, expand=False, fill=True, padding=20)
        page4_vp.add(pg4_box)
        page4.add(page4_vp)
        notebook.append_page(page4, tab4_lbl)


        page5 = Gtk.ScrolledWindow.new()
        page5_vp = Gtk.Viewport.new()
        tab5_lbl = Gtk.Label.new(_('About'))
        pg5_box = Gtk.Box.new(orientation=Gtk.Orientation.VERTICAL, spacing = 20)

        pg5_lbl1 = Gtk.Label.new("")
        pg5_lbl1.set_markup("<b>TableGimper</b>\n\n"
                            "<b>Version </b>" + str(version))
        pg5_lbl1.set_halign(Gtk.Align.CENTER)
        pg5_lbl1.set_justify(Gtk.Justification.CENTER)
        pg5_lbl1.set_margin_start(20)
        pg5_lbl1.set_selectable(True)
        pg5_box.pack_start(pg5_lbl1, expand=False, fill=True, padding=20)

        logo = Gtk.Image.new_from_file(os.path.join(script_dir,"images/logo.png"))
        pg5_box.pack_start(logo, expand=False, fill=True, padding=0)
        pg5_lbl2 = Gtk.Label.new("")
        pg5_lbl2.set_markup(_("<b>Author: </b>") + "Caetano Veyssières\n\n" +
                            _("<b>With help from: </b>") + "tmanni, Akkana Peck (akk), Ofnuts, eepjr24\n\n" +
                            _("<b>Release date: </b>") + release_date + "\n\n" +
                            _("<b>Online repository: </b>") + "https://gitlab.com/ChameleonScales/tablegimper")
        pg5_lbl2.set_halign(Gtk.Align.CENTER)
        pg5_lbl2.set_justify(Gtk.Justification.CENTER)
        pg5_lbl2.set_margin_start(20)
        pg5_lbl2.set_selectable(True)
        pg5_box.pack_start(pg5_lbl2, expand=False, fill=True, padding=20)
        page5_vp.add(pg5_box)
        page5.add(page5_vp)
        notebook.append_page(page5, tab5_lbl)

        area = self.get_content_area()
        area.pack_start(notebook, expand=True, fill=True, padding=0)
        self.show_all()

    def make_table(self, csv_file, table_box, table):
        with open(os.path.join(script_dir, csv_file),'r') as csvfile:
            tableReader = csv.reader(csvfile, delimiter='\t')
            row_id = 0
            for row in tableReader:
                if row :
                    col_id = 0
                    for cell in row:
                        cell_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=15)
                        cell_label = Gtk.Label.new('')
                        cell_label.set_selectable(True)
                        cell_label.set_margin_start(10)
                        cell_label.set_margin_end(10)
                        cell_label.set_margin_top(10)
                        cell_label.set_margin_bottom(10)
                        cell_box.pack_start(cell_label, expand=True, fill=True, padding=0)
                        cell_label.set_halign(Gtk.Align.START)
                        table.attach(  child = cell_box,
                                            left_attach = col_id,
                                            right_attach = col_id+1,
                                            top_attach = row_id,
                                            bottom_attach = row_id+1,
                                            xoptions = Gtk.AttachOptions.FILL,
                                            yoptions = Gtk.AttachOptions.FILL,
                                            xpadding = 0,
                                            ypadding = 0    )
                        cell_context = Gtk.Widget.get_style_context(cell_box)
                        if '[' and ']' in cell:
                            bgd_col = cell.split(']')[0].split('[')[1]
                            cell = cell.split(']')[1]
                        else:
                            bgd_col = ''
                        Gtk.StyleContext.add_class(cell_context, "cell_" + bgd_col)
                        cell_label.set_markup(cell)
                        col_id += 1
                row_id += 1
        cell_context = Gtk.Widget.get_style_context(table_box)
        Gtk.StyleContext.add_class(cell_context, "help_table_box")
