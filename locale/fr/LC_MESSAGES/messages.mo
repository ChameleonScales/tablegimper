��    :      �  O   �      �  N   �  I   H  c   �  
   �            
             '     0     >     F  �  V  �   
  :  �
     ,     G  z  ]  �   �  �   �     2     J     P  A   m     �     �     �  !   �          (  :   8     s     �     �  G   �               #     8     E     c     }     �  
   �  G   �        
          B   (  
   k     v     �     �     �     �     �  7     �  ?  a   �  V   S  i   �          "     (  
   0     ;  	   J     T     e     n  1    �   �  �  �      �%     �%    �%  �   �)  �   l*     *+  	   C+     M+  A   f+     �+     �+     �+      �+     ,     *,  ;   A,     },     �,     �,  Q   �,     %-     4-     I-     _-  '   p-     �-     �-  -   �-      .  I   .     W.     c.     t.  D   �.  
   �.     �.     �.     /  %   $/  %   J/     p/  8   �/              +   '                    0   #   7   1   ,                               (       5   9         :                 *   %       8                    -   "   &                                           4   .          6                    
      3   	   2      /      )   !   $    


csv files already existed.
The old ones were saved with the .bak extension. 

Please make the necessary corrections or simply re-generate orig_table. 

does not exist.

Please chose a folder containing the files:
xcf_orig_table.csv
xcf_new_table.csv  Browse...  Help  Run  contains   doesn't exist.  layers
  layers, not   lines
 <b>Author: </b> <b>In this treatment mode, you can use a table to create new xcfs and/or modify the contents of existing xcfs.</b>

Steps to follow :
   1. Generate the tables with the dedicated action
   2. If some lines don't require any modification, remove them from both tables
   3. Edit new_table.csv according to your desired modifications.

The following example shows the formatting of all possible treatments (the colors are just there for explanation):
   • <span foreground="#1698E9"><b>Blue</b></span> : Image files to import as layers. <b>Note:</b> they must be in the same folder as the xcfs they are imported into.
   • <span foreground="#25DC9A"><b>Teal</b></span> : Non-existing xcfs to be created
   • <span background="#ffccf7" foreground="black"> Pink background </span> : Erased layer to be deleted (here B1)
   • <span foreground="#fc5c00"><b>{</b></span>In curly brackets<span foreground="#fc5c00"><b>}</b></span> : Add a copy of another layer's mask. <b>Note:</b> The 'Add mask' checkbox in tableGimper not only adds the extracted channel as a mask but also as a layer.
           This is for convenience, to prepare your manual masking workflow a little more. <b>Notes :</b>
   ▪ As you can see, <span foreground="#1698E9"><b>B2.png</b></span> can take <span foreground="#fc5c00"><b>{</b></span>B1<span foreground="#fc5c00"><b>}</b></span>'s mask despite B1 being <span background="#ffccf7" foreground="black"> erased </span>.
   ▪ If you import a file in place of an existing layer:
           • The existing mask will stay in place (see figure below)
           • Therefore, you don't need to add its name <span foreground="#fc5c00"><b>{</b></span>in curly brackets<span foreground="#fc5c00"><b>}</b></span>
           • You can however forcefully use another layer's mask with the brackets
           • If you don't want to use any mask:
                   ◦ Instead of replacing the layer, empty its cell and insert the new image
                       in an available space to the right of the table.
   ▪ A layer can be replaced by a file only if the file name differs from all the existing layers.
   ▪ You can switch a layer's visibility by changing the 👁 or ◡ symbol.

<b>All the possible layer movements :</b> <b>Online repository: </b> <b>Release date: </b> <b>This action is the first step necessary to any treatment.</b>

It searches for .xcf files from the root folder, with the given search depths.
It then generates 2 identical tables:
   <b>• xcf_new_table.csv</b>, which will later represent the changes you want to make
   <b>• xcf_orig_table.csv</b>, which represents the original state of your xcfs.
     You may not edit this file unless you want to discard entire lines.
     It is only used by tableGimper as a safety check to ensure the changes you made to new_table.csv
     are based on the current contents of the xcfs.

If no .xcf was found during this action, empty files will be created. You can use them later to create new xcfs.

The 👁 and ◡ symbols indicate the visibility of a layer.
The column separator is the tabulation.

<b>An example of a generated table (the header line is not included in the csv files):</b> <b>This mode allows you to export xcfs contained in new_table.csv as jpg files.</b>

orig_table is ignored and only the first column of new_table is used in this mode. <b>This mode allows you to rename layers as well as change their visibility.</b>

Once the tables are generated, change the layer names in new_table.csv and run this treatment.
 <b>With help from: </b> About According to xcf_new_table,  Add mask if first layer doesn't have one (extracted blue channel) Badly named masks:

 Batch jpg export finished. Batch renaming finished. Batch xcf modifications finished. Export to jpg Generate tables Modification aborted.
xcf_new_table contains these errors: Modification aborted:

 Modify layers and masks Non-existing image files:

 Non-existing referred masks
(the referred layer doesn't have a mask):

 Quality (%) : Rename layers Renaming aborted :

 Root folder: Searching depths (0 = root) : TableGimper : Root folder TableGimper : Warnings The error was logged in  The file:
 The following masks don't exist because
the xcf itself doesn't exist:

 The layer [ Treat xcfs Treatment type : Visibility (👁 or ◡) not indicated
for the following layers:

 Waiting... ] should be named [ csv tables created in:
 images/movements.png while in reality it contains  while xcf_orig_table has  xcf_new_table contains  xcf_orig_table contains at least the following error:

 Project-Id-Version: 
PO-Revision-Date: 2021-11-19 00:32+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: start_ui.py
X-Poedit-SearchPath-1: help_dialog.py
X-Poedit-SearchPath-2: tabler.py
X-Poedit-SearchPath-3: gimper.py
 


Des csv étaient déjà présents.
Les originaux ont été sauvegardés avec l'extension .bak. 

Veuillez faire les corrections nécessaires ou simplement regénérer un orig_table. 

n'existe pas.

Veuillez choisir un chemin contenant les fichiers :
xcf_orig_table.csv
xcf_new_table.csv  Parcourir...  Aide  Lancer  possède   n'existe pas.  calques
  calques et non   lignes
 <b>Auteur : </b> <b>Dans ce mode de traitement, vous pouvez utiliser un tableau pour créer de nouveaux xcfs et/ou modifier le contenu d'xcfs existants.</b>

Étapes à suivre :
   1. Générez les tableaux avec l'action dédiée
   2. Si des lignes ne nécessitent aucune modification, retirez-les des 2 tableaux
   3. Modifiez new_table.csv selon les modifications souhaitées.

L'exemple ci-dessous montre le formalisme des traitements possibles (les couleurs ne servent qu'à l'explication):
   • <span foreground="#1698E9"><b>Bleu</b></span> : fichiers images à importer en tant que calques. <b>Note :</b> ils doivent se trouver dans les mêmes dossiers que les xcfs dans lesquels ils sont importés.
   • <span foreground="#25DC9A"><b>Turquoise</b></span> : xcfs non existants à créer
   • <span background="#ffccf7" foreground="black"> Fond rose </span> : case effacée, calque à supprimer (ici B1)
   • <span foreground="#fc5c00"><b>{</b></span>Entre accolades<span foreground="#fc5c00"><b>}</b></span> : Récupérer une copie d'un masque appartenant à un autre calque. <b>Note:</b> La case à cocher 'Nouveau masque' dans tableGimper ajoute non seulement la couche extraite dans un masque mais aussi dans un nouveau calque.
           Ceci sert à faciliter la préparation de votre étape de masquage manuel. <b>Notes :</b>
   ▪ Comme vous le voyez, <span foreground="#1698E9"><b>B2.png</b></span> peut récupérer le masque de <span foreground="#fc5c00"><b>{</b></span>B1<span foreground="#fc5c00"><b>}</b></span> malgré que B1 ait été <span background="#ffccf7" foreground="black"> effacé </span>.
   ▪ Si vous importez un fichier en écrasant une case existante:
           • Le masque sera automatiquement récupéré du calque écrasé (cf schéma ci-dessous)
           • Inutile donc d'ajouter son nom <span foreground="#fc5c00"><b>{</b></span>entre accolades<span foreground="#fc5c00"><b>}</b></span>
           • Vous pouvez cependant forcer la récupération d'un autre masque avec les accolades
           • Si vous ne souhaitez pas récupérer le masque du calque remplacé :
                   ◦ Plutôt que d'écraser le calque, videz la case que vous souhaitiez écraser,
                       puis insérez la nouvelle image dans un emplacement libre à droite du tableau.
   ▪ Un calque ne sera remplacé par un fichier que si le nom du fichier diffère de tous les calques existants.
   ▪ Vous pouvez inverser la visibilité d'un calque en changeant le symbole 👁 ou ◡.

<b>Schéma des mouvements possibles :</b> <b>Dépôt en ligne : </b> <b>Date de sortie : </b> <b>Cette action est la première étape nécessaire à tout traitement.</b>

Il cherche les fichiers .xcf à partir du dossier racine, avec les profondeurs de recherche indiquées.
Il génère ensuite 2 tableaux identiques :
   <b>• xcf_new_table.csv</b>, qui représentera les modifications que vous souhaitez apporter
   <b>• xcf_orig_table.csv</b>, qui représente l'état original de vos xcfs.
     Vous ne devez pas modifier ce fichier, sauf si vous souhaitez ignorer des lignes entières.
     Il n'est utilisé par TableGimper que par mesure de sécurité pour s'assurer que les modifications
     apportées à new_table.csv sont basées sur le contenu actuel des xcfs.

S'il aucun .xcf n'a été trouvé durant cette action, des fichiers vides seront créés. Vous pourrez les utiliser pour créer de nouveaux xcfs.

Les symboles 👁 ou ◡ indiquent la visibilité du calque.
Le séparateur de colonne est la tabulation.

<b>Un exemple de tableau généré (la ligne d'en-tête n'est pas présente dans les csv):</b> <b>Ce mode permet d'exporter en jpg les xcfs contenus dans new_table.csv.</b>

orig_table est ignoré et seule la première colonne de new_table est utilisée dans ce mode. <b>Ce mode permet de renommer des calques et de changer leur visibilité.</b>

Une fois les tableaux générés, modifiez les noms de calques dans new_table.csv et exécutez ce traitement.
 <b>Avec l'aide de : </b> À propos D'après xcf_new_table,  Nouveau masque si aucun dans le 1er calque (extrait couche bleue) Masques mal nommés :

 Export des jpg terminé. Renommage des calques terminé. Modification des xcfs terminée. Exporter en jpg Générer des tableaux Modification avortée.
xcf_new_table contient ces erreurs : Modification avortée :

 Modifier calques et masques Fichiers images inexistants :

 Masques référés inexistants
le calque référé ne possède pas de masque) :

 Qualité (%) : Renommer des calques Renommage avorté :

 Dossier racine : Profondeurs de recherche (0 = racine) : TableGimper : Dossier racine TableGimper : Avertissements Le message d'erreur a été sauvegardé dans  Le fichier:
 Les masques suivants n'existent pas car
le xcf lui-même n'existe pas :

 Le calque [ Traiter des xcfs Type de traitement : Visibilité (👁 ou ◡) non indiquée
pour les calques suivants :
 Attente... ] devrait s'appeler [ Tableaux csv créés dans :
 images/movements_fr.png alors qu'en réalité il en contient  alors que xcf_orig_table en contient  xcf_new_table contient  xcf_orig_table contient au minimum l'erreur suivante :

 