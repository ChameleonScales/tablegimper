#!/usr/bin/env python2.7
# -*- coding: UTF-8 -*-

from gimpfu import *
#from gimpfu import pdb, gimp, TRUE, FALSE, CHANNEL_BLUE
import sys, os, os.path, csv, gtk, pango, threading, gobject, logging, traceback, time, re, gettext

t = gettext.translation(domain='messages', localedir='locale', fallback=True)
_ = t.ugettext

root_dir = os.environ['root_dir']
orig_csv_abs = os.environ['orig_csv_abs']
new_csv_abs = os.environ['new_csv_abs']
treatment_type = os.environ['treatment_type']

def parse_boolean(string):
    return string == 'True'
if treatment_type == 'treat_l_m':
    make_masks = parse_boolean(os.environ['make_masks'])
elif treatment_type == 'treat_export':
    jpg_quality = float(os.environ['jpg_quality'])/100

LOG_FILENAME = '/tmp/TableGimper_errors.log'
FORMAT = "\n\n\n%(asctime)-15s \n\n%(message)s"
logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG, format=FORMAT, datefmt='%Y %b %d %H:%M:%S', maxBytes=32000)



# This class lets a threaded process log its errors
# see https://stackoverflow.com/a/9929970 for more ways to handle threaded error logging
class TracebackLoggingThread(threading.Thread):
    def run(self):
        try:
            super(TracebackLoggingThread, self).run()
        except (KeyboardInterrupt, SystemExit):
            raise
        except Exception as e:
            logging.error(e, exc_info = True)
            PWin.icon.set_from_stock(gtk.STOCK_DIALOG_ERROR, 6)
            PWin.state_lbl_box.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))
            traceback_lbl = "Error :\n\n" + traceback.format_exc()
            PWin.state_lbl.set_text(traceback_lbl)
            PWin.state_lbl.modify_fg(gtk.STATE_NORMAL, gtk.gdk.color_parse("red"))
            PWin.state_lbl.set_selectable(True)
            PWin.icon_box.show()
            PWin.state_lbl_box.show()
            PWin.err_saved_box.show()

class ProgressWindow(gtk.Window):
    def __init__(self):
        gtk.Window.__init__(self)
        self.connect("destroy", gtk.main_quit)
        self.set_title("TableGimper")
        self.set_default_size(500,100)
        main_box = gtk.VBox()
        self.add(main_box)

        self.progressbar = gtk.ProgressBar()
        #self.progressbar.set_fraction(0)
        self.progressbar.set_text(_("Loading..."))
        progressbar_box = gtk.HBox(False, 20)
        progressbar_box.set_border_width(20)
        main_box.pack_start(progressbar_box, False, False, 20)
        progressbar_box.pack_start(self.progressbar)

        self.icon_box = gtk.VBox()
        self.icon = gtk.Image()
        self.icon.set_from_stock(gtk.STOCK_APPLY, 6)
        #self.icon.set_padding(15,15)
        self.icon_box.pack_start(self.icon, False, False, 0)
        main_box.pack_start(self.icon_box, False, False, 0)

        self.state_lbl_box = gtk.EventBox()
        self.state_lbl_box.set_border_width(15)
        main_box.pack_start(self.state_lbl_box, False, False, 10)

        self.state_lbl = gtk.Label("Everything great so far...")
        self.state_lbl.set_padding(20,20)
        self.state_lbl.modify_fg(gtk.STATE_NORMAL, gtk.gdk.color_parse("green"))
        self.state_lbl_box.add(self.state_lbl)

        self.err_saved_box = gtk.VBox()
        self.err_saved = gtk.Label(_('The error was logged in ') + LOG_FILENAME)
        self.err_saved_box.pack_start(self.err_saved, False, False, 40)
        main_box.pack_start(self.err_saved_box, False, False, 0)

        #time.sleep(2) # this is to prevent the progress bar from freezing in the beginning
        if treatment_type == 'treat_l_m':
            t = TracebackLoggingThread(target = self.modify_images)
        elif treatment_type == 'treat_rename':
            t = TracebackLoggingThread(target = self.rename_layers)
        elif treatment_type == 'treat_export':
            t = TracebackLoggingThread(target = self.export_jpgs)
        t.start()

    def export_jpgs(self):
        gobject.threads_init()
        new_dict = csv_to_dict(new_csv_abs)
        tasks = len(new_dict)
        task_id = 0
        prog_fraction = 0
        time.sleep(0.1) # this is to prevent the progress bar from freezing in the beginning
        for xcf_rel_url in sorted(new_dict):
            time.sleep(0.05) # this is to prevent the progress bar from freezing in the beginning
            task_id += 1
            self.progressbar.set_fraction(prog_fraction)
            self.progressbar.set_text(str(task_id) + '/' + str(tasks) + ' - ' + xcf_rel_url)
            xcf_abs_url = os.path.join(root_dir, xcf_rel_url)
            xcf_dir = os.path.dirname(xcf_abs_url)
            out_img_path = os.path.splitext(xcf_abs_url)[0] + ".jpg"
            image = pdb.gimp_file_load(xcf_abs_url, os.path.basename(xcf_rel_url))
            layer = pdb.gimp_image_flatten(image)
            pdb.file_jpeg_save( image, layer, out_img_path, os.path.basename(out_img_path),
                                jpg_quality, 0, 1, 0, "", 2, 1, 0, 0)
            gimp.delete(image)
            prog_fraction += float(1)/tasks

        self.progressbar.set_fraction(prog_fraction)
        self.progressbar.set_text(str(task_id) + '/' + str(tasks) + ' - ' + xcf_rel_url)
        self.icon_box.show()
        self.state_lbl.set_text(_("Batch jpg export finished."))
        PWin.state_lbl_box.show()


    def rename_layers(self):
        gobject.threads_init()
        orig_dict = csv_to_dict(orig_csv_abs)
        new_dict = csv_to_dict(new_csv_abs)
        tasks = len(new_dict)
        task_id = 0
        prog_fraction = 0
        time.sleep(0.1) # this is to prevent the progress bar from freezing in the beginning
        for xcf_rel_url in sorted(new_dict):
            time.sleep(0.05) # this is to prevent the progress bar from freezing in the beginning
            task_id += 1
            self.progressbar.set_fraction(prog_fraction)
            self.progressbar.set_text(str(task_id) + '/' + str(tasks) + ' - ' + xcf_rel_url)
            xcf_abs_url = os.path.join(root_dir, xcf_rel_url)

            has_changes = False
            layer_id = 0
            for layer in new_dict[xcf_rel_url]:
                if layer['layer'][0] != orig_dict[xcf_rel_url][layer_id]['layer'][0] or layer['visi'] != orig_dict[xcf_rel_url][layer_id]['visi']:
                    has_changes = True
                    break
                else:
                    layer_id +=1

            if has_changes:
                image = pdb.gimp_file_load(xcf_abs_url, os.path.basename(xcf_rel_url))
                layer_id = 0
                for layer in reversed(image.layers):
                    layer.name = new_dict[xcf_rel_url][layer_id]['layer'][0]
                    layer.visible = new_dict[xcf_rel_url][layer_id]['visi']
                    layer_id += 1
                first_layer_name = new_dict[xcf_rel_url][0]['layer'][0]
                active_layer = pdb.gimp_image_get_layer_by_name(image, first_layer_name)
                pdb.gimp_xcf_save(0, image, active_layer, xcf_abs_url, xcf_rel_url)

            prog_fraction += float(1)/tasks
        self.progressbar.set_fraction(prog_fraction)
        self.progressbar.set_text(str(task_id) + '/' + str(tasks) + ' - ' + xcf_rel_url)
        self.icon_box.show()
        self.state_lbl.set_text(_("Batch renaming finished."))
        PWin.state_lbl_box.show()

    def modify_images(self):
        gobject.threads_init()
        orig_dict = csv_to_dict(orig_csv_abs)
        new_dict = csv_to_dict(new_csv_abs)
        warn_xcf_not_created = 'xcfs not created because first image not found:\n\n'
        warn_xcf_not_created_list = ''
        warn_img_file_absent = 'Image files not found:\n\n'
        warn_img_file_absent_list = ''
        warn_mask_absent = "Non-existing referred masks\n(the referred layer doesn't have a mask):\n\n"
        warn_mask_absent_list = ''
        warn_visi = 'Visibility (👁 or ◡) not indicated\nfor the following layers\n(they have been set to visible as a fallback):\n\n'
        warn_visi_list = ''
        warn_mask_badname = "Badly named masks:\n\n"
        warn_mask_badname_list = ''
        warn_mask_noxcf = "The following masks don't exist because\nthe xcf itself doesn't exist:\n\n"
        warn_mask_noxcf_list = ''
        tasks = len(new_dict)
        task_id = 0
        prog_fraction = 0
        time.sleep(0.1) # this is to prevent the progress bar from freezing in the beginning
        for xcf_rel_url in sorted(new_dict):
            time.sleep(0.05) # this is to prevent the progress bar from freezing in the beginning
            task_id += 1
            self.progressbar.set_fraction(prog_fraction)
            self.progressbar.set_text(str(task_id) + '/' + str(tasks) + ' - ' + xcf_rel_url)
            xcf_abs_url = os.path.join(root_dir, xcf_rel_url)

            if os.path.exists(xcf_abs_url):
                orig_xcf_exists = True
                row_processable = True
                image = pdb.gimp_file_load(xcf_abs_url, os.path.basename(xcf_rel_url))
            else:
                # -------------------------------------------------------------------------------------------------
                # create new xcf:
                orig_xcf_exists = False
                bgd_img_file_base = new_dict[xcf_rel_url][0]['layer'][0]
                bgd_img_file_abs = os.path.join(root_dir, os.path.dirname(xcf_rel_url), bgd_img_file_base)
                if os.path.exists(bgd_img_file_abs):
                    row_processable = True
                    image = pdb.gimp_file_load(bgd_img_file_abs, bgd_img_file_base)
                else:
                    row_processable = False
                    warn_xcf_not_created_list += '• ' + xcf_rel_url + ' ------> not found: ' + bgd_img_file_base + '\n'
            if row_processable :
                pdb.gimp_selection_none(image)
                img_width = pdb.gimp_image_width(image)
                img_height = pdb.gimp_image_height(image)
                layer_id = 0
                for new_layer_data in new_dict[xcf_rel_url]:
                    new_layer_name = new_layer_data['layer'][0]
                    new_mask_name = new_layer_data['mask'][0]
                    new_layer_in_orig = False
                    new_mask_in_orig = False
                    if new_layer_name : # cell is not empty
                        if orig_xcf_exists :
                            for orig_layer_data in orig_dict[xcf_rel_url]:
                                orig_layer_name = orig_layer_data['layer'][0]
                                orig_layer = pdb.gimp_image_get_layer_by_name(image, orig_layer_name)

                                # -------------------------------------------------------------------------------------------------
                                # Store orig layer :
                                if new_layer_name == orig_layer_name:
                                    new_layer_in_orig = True
                                    new_layer_data['layer'].append(pdb.gimp_layer_copy(orig_layer, True))

                                    # -------------------------------------------------------------------------------------------------
                                    # Store unspecified orig mask :
                                    if new_mask_name is None and orig_layer.mask:
                                        store_mask(orig_layer, image, img_width, img_height, new_layer_data)

                                # -------------------------------------------------------------------------------------------------
                                # Store specified orig mask :
                                if new_mask_name == orig_layer_name:
                                    new_mask_in_orig = True
                                    if orig_layer.mask: # if orig layer has a mask
                                        store_mask(orig_layer, image, img_width, img_height, new_layer_data)
                                    else:
                                        warn_mask_absent_list += '• ' + new_layer_name + '{' + orig_layer_name + '} ------> line : ' + xcf_rel_url + '\n'


                        # -------------------------------------------------------------------------------------------------
                        # Store new image file :
                        if not new_layer_in_orig:
                            new_image_file = os.path.join(root_dir, os.path.dirname(xcf_rel_url), new_layer_name)
                            if os.path.exists(new_image_file) :
                                new_layer_data['layer'].append(pdb.gimp_file_load_layer(image, new_image_file))
                            else:
                                warn_img_file_absent_list += '• ' + new_image_file + '\n'
                            if orig_xcf_exists :
                                # -------------------------------------------------------------------------------------------------
                                # Store unspecified orig mask : (we do it again because this time we're in "if not new_layer_in_orig")
                                new_cellId_in_orig = len(orig_dict[xcf_rel_url]) >= layer_id+1
                                if new_mask_name is None and new_cellId_in_orig:
                                    orig_layer_name = orig_dict[xcf_rel_url][layer_id]['layer'][0]
                                    orig_layer = pdb.gimp_image_get_layer_by_name(image, orig_layer_name)
                                    if orig_layer.mask :
                                        store_mask(orig_layer, image, img_width, img_height, new_layer_data)

                        # -------------------------------------------------------------------------------------------------
                        # Warn bad mask name :
                        if new_mask_name is not None and not new_mask_in_orig :
                            if orig_xcf_exists:
                                warn_mask_badname_list += '• {' + new_mask_name + '} ------> line : ' + xcf_rel_url + '\n'
                            else:
                                warn_mask_noxcf_list += '• {' + new_mask_name + '} ------> ' + xcf_rel_url + '\n'
                        # -------------------------------------------------------------------------------------------------
                        # Warn unindicated visibility :
                        if new_layer_data['visi'] is None:
                            warn_visi_list += '• ' + new_layer_name + ' ------> ligne : ' + xcf_rel_url + '\n'

                    layer_id += 1

                apply_changes(image, new_dict, xcf_rel_url, xcf_abs_url, make_masks)
                gimp.delete(image)
            prog_fraction += float(1)/tasks

        self.progressbar.set_fraction(prog_fraction)
        self.progressbar.set_text(str(task_id) + '/' + str(tasks) + ' - ' + xcf_rel_url)
        self.icon_box.show()
        self.state_lbl.set_text(_("Batch xcf modifications finished."))
        PWin.state_lbl_box.show()


        # -------------------------------------------------------------------------------------------------
        # Warnings:
        w1_bool = bool(warn_xcf_not_created_list)
        w2_bool = bool(warn_img_file_absent_list)
        w3_bool = bool(warn_mask_absent_list)
        w4_bool = bool(warn_mask_badname_list)
        w5_bool = bool(warn_mask_noxcf_list)
        w6_bool = bool(warn_visi_list)
        w_bools = [w1_bool, w2_bool, w3_bool, w4_bool, w5_bool, w6_bool]

        w1_txt = warn_xcf_not_created + warn_xcf_not_created_list
        w2_txt = warn_img_file_absent + warn_img_file_absent_list
        w3_txt = warn_mask_absent + warn_mask_absent_list
        w4_txt = warn_mask_badname + warn_mask_badname_list
        w5_txt = warn_mask_noxcf + warn_mask_noxcf_list
        w6_txt = warn_visi + warn_visi_list
        w_txts = [w1_txt, w2_txt, w3_txt, w4_txt, w5_txt, w6_txt]

        warnings_dict = {   'w1':[w1_bool,w1_txt],
                            'w2':[w2_bool,w2_txt],
                            'w3':[w3_bool,w3_txt],
                            'w4':[w4_bool,w4_txt],
                            'w5':[w5_bool,w5_txt],
                            'w6':[w6_bool,w6_txt]}
        if any(w_bools):
            avert = Warnings(warnings_dict)
            avert.run()
            avert.destroy()






# -------------------------------------------------------------------------------------------------------------------------
# Structure of the dictionary:
#
# {xcf_relative_url: [
#                         {
#                             'layer': ['layer_name', <layer_content>],
#                             'mask': ['mask_name', <mask_content>],
#                             'visi': boolean
#                         }
#                         ...
#                         ...
#                         ...
#                     ]

def csv_to_dict(abs_url):
    dict = {}
    with open(abs_url,'r') as csvfile:
        tableReader = csv.reader(csvfile, delimiter='\t')
        for row in tableReader:
            if row :
                dict[row[0]] = []
                for duo in [row[n:n+2] for n in range(1, len(row), 2)]: # splits the row into duets of cells (visi and layer)
                    layer_dict = {}
                    dict[row[0]].append(layer_dict)
                    for idx, cell in enumerate(duo):
                        # -------------------------------------------------------------------------------------------------
                        # append visibility:
                        if idx == 0 : # if first item of duo
                            if '◡' in cell:
                                visibility = False
                            elif '👁' in cell:
                                visibility = True
                            else:
                                visibility = None
                            layer_dict['visi'] = visibility

                        # -------------------------------------------------------------------------------------------------
                        # append layer and mask:
                        elif idx == 1 : # second item of duo
                            if '{' and '}' in cell :
                                layer_name = cell[:cell.find('{')]
                                mask_name = cell[cell.find('{')+1:cell.find('}')]
                                layer_dict['layer'] = [layer_name]
                                layer_dict['mask'] = [mask_name]
                            else :
                                layer_dict['layer'] = [cell]
                                layer_dict['mask'] = [None]
    return dict

def store_mask(orig_layer, image, img_width, img_height, new_layer_data):
    orig_mask = orig_layer.mask
    pdb.gimp_edit_copy(orig_mask)
    channel = pdb.gimp_channel_new(image, img_width, img_height, orig_layer.name, 100, (255, 255, 255))
    pdb.gimp_image_insert_channel(image, channel, None, 0)
    floating_sel = pdb.gimp_edit_paste(channel, True)
    pdb.gimp_floating_sel_anchor(floating_sel)
    new_layer_data['mask'].append(channel)


def apply_changes(image, new_dict, xcf_rel_url, xcf_abs_url, make_masks):
    for layer in image.layers:
        pdb.gimp_image_remove_layer(image, layer)
    for new_layer_data in new_dict[xcf_rel_url]:
        new_layer_visi = new_layer_data['visi']
        new_layer_name = new_layer_data['layer'][0]
        new_mask_name = new_layer_data['mask'][0]
        if new_layer_name and len(new_layer_data['layer']) > 1 : # cell is not empty and layer was found
            layer = new_layer_data['layer'][1]
            pdb.gimp_image_insert_layer(image, layer, None, 0)
            layer.name = new_layer_name
            if layer.mask:
                pdb.gimp_layer_remove_mask(layer, 1)
            if len(new_layer_data['mask']) > 1: # if there is a stored mask
                stored_channel = new_layer_data['mask'][1]
                pdb.gimp_edit_copy(stored_channel)
                new_mask = layer.create_mask(0)
                layer.add_mask(new_mask)
                floating_sel = pdb.gimp_edit_paste(new_mask, True)
                pdb.gimp_floating_sel_anchor(floating_sel)

            # -------------------------------------------------------------------------------------------------
            # Set visibility:
            if new_layer_visi is None :
                new_layer_visi = True
            layer.visible = new_layer_visi

    # -------------------------------------------------------------------------------------------------
    # Delete stored channels:
    for new_layer_data in new_dict[xcf_rel_url]:
        if len(new_layer_data['mask']) > 1: # if there is a stored mask
            channel = new_layer_data['mask'][1]
            pdb.gimp_image_remove_channel(image, channel)

    # -------------------------------------------------------------------------------------------------
    # generate blue mask and layer :

    if make_masks:
        second_layer = new_dict[xcf_rel_url][1]['layer'][1]
        if not second_layer.mask :
            first_layer = new_dict[xcf_rel_url][0]['layer'][1]
            pdb.gimp_image_raise_item_to_top(image, first_layer)
            blue_channel = pdb.gimp_channel_new_from_component(image, CHANNEL_BLUE, 'blue')
            pdb.gimp_image_insert_channel(image, blue_channel, None, 0)
            pdb.gimp_edit_copy(blue_channel)
            pdb.gimp_image_remove_channel(image, blue_channel)
            
            blue_layer = pdb.gimp_layer_copy(first_layer, False)
            pdb.gimp_image_insert_layer(image, blue_layer, None, 0)
            blue_layer.visible = False
            layer_paste = pdb.gimp_edit_paste(blue_layer, True)
            pdb.gimp_floating_sel_anchor(layer_paste)
            blue_layer.name = first_layer.name + "_blue"
            
            blue_mask = second_layer.create_mask(0)
            second_layer.add_mask(blue_mask)
            mask_paste = pdb.gimp_edit_paste(blue_mask, True)
            pdb.gimp_floating_sel_anchor(mask_paste)
            pdb.gimp_layer_set_show_mask(second_layer, True)

            pdb.gimp_image_lower_item_to_bottom(image, blue_layer)
            pdb.gimp_image_lower_item_to_bottom(image, first_layer)


    # -------------------------------------------------------------------------------------------------
    # Backup the old xcf and save the new one:
    if os.path.exists(xcf_abs_url):
        xcf_backup = xcf_abs_url + '.bak'
        os.rename(xcf_abs_url, xcf_backup)
    first_layer_name = new_dict[xcf_rel_url][0]['layer'][0]
    active_layer = pdb.gimp_image_get_layer_by_name(image, first_layer_name)
    pdb.gimp_xcf_save(0, image, active_layer, xcf_abs_url, xcf_rel_url)
    # Other possibility :
    #new_xcf_abs_url = xcf_abs_url.split('.xcf')[0] + '_new.xcf'
    #new_xcf_rel_url = xcf_rel_url.split('.xcf')[0] + '_new.xcf'
    #pdb.gimp_xcf_save(0, image, active_layer, new_xcf_abs_url, new_xcf_rel_url)

class Warnings(gtk.Dialog):
    def __init__(self, warnings_dict):
        dialog = gtk.Dialog.__init__(self)
        area = self.get_content_area()
        self.set_title("TableGimper : Warnings")
        self.add_button(gtk.STOCK_OK, gtk.RESPONSE_OK)
        self.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("grey"))
        top_label = gtk.Label(  "This message should not appear.\n" +
                                "You either modified xcfs while tableGimper\n" +
                                "was running or just found a bug.\n" +
                                "Please report it on\n" +
                                "https://gitlab.com/ChameleonScales/tablegimper/-/issues")
        top_label.modify_fg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))
        top_label.set_padding(15,30)
        top_style = top_label.get_style()
        top_font_family = top_style.font_desc.get_family()
        orig_top_font_size = pango.FontDescription.get_size(top_style.font_desc)
        new_top_font_size = (orig_top_font_size/1000) * 2
        new_font = pango.FontDescription(top_font_family + " " + str(new_top_font_size))
        top_label.modify_font(new_font)
        area.add(top_label)
        box = gtk.HBox()
        box.set_border_width(0)
        self.connect("destroy", gtk.main_quit)
        if warnings_dict['w1'][0]:
            w1 = warn_msg(gtk.STOCK_DIALOG_WARNING, warnings_dict['w1'][1], "red")
            box.pack_start(w1.msg_box)
        if warnings_dict['w2'][0]:
            w2 = warn_msg(gtk.STOCK_DIALOG_WARNING, warnings_dict['w2'][1], "red")
            box.pack_start(w2.msg_box)
        if warnings_dict['w3'][0]:
            w3 = warn_msg(gtk.STOCK_DIALOG_WARNING, warnings_dict['w3'][1], "red")
            box.pack_start(w3.msg_box)
        if warnings_dict['w4'][0]:
            w4 = warn_msg(gtk.STOCK_DIALOG_WARNING, warnings_dict['w4'][1], "red")
            box.pack_start(w4.msg_box)
        if warnings_dict['w5'][0]:
            w5 = warn_msg(gtk.STOCK_DIALOG_WARNING, warnings_dict['w5'][1], "red")
            box.pack_start(w5.msg_box)
        if warnings_dict['w6'][0]:
            w6 = warn_msg(gtk.STOCK_DIALOG_INFO, warnings_dict['w6'][1], "black")
            box.pack_start(w6.msg_box)
        area.add(box)
        self.show_all()

class warn_msg(gtk.Box):
    def __init__(self, icon, text, color):
        self.msg_box = gtk.EventBox()
        self.msg_box.set_border_width(10)
        self.msg_box.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))
        container = gtk.VBox()
        container.set_border_width(20)
        msg_icon = gtk.Image()
        msg_icon.set_from_stock(icon, 6)
        msg_label = gtk.Label(text)
        msg_label.set_selectable(True)
        msg_label.set_padding(0,15)
        msg_label.modify_fg(gtk.STATE_NORMAL, gtk.gdk.color_parse(color))
        container.pack_start(msg_icon)
        container.pack_start(msg_label)
        self.msg_box.add(container)

PWin = ProgressWindow()
PWin.connect("delete-event", gtk.main_quit)
PWin.show_all()
PWin.icon_box.hide()
PWin.state_lbl_box.hide()
PWin.err_saved_box.hide()
PWin.resize(500,1)
gtk.main()
