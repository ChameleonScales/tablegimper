#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys, os, os.path, gi, csv, fnmatch, gettext

t = gettext.translation(domain='messages', localedir='locale', fallback=True)
_ = t.gettext

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
from shutil import copyfile
script_dir = os.path.dirname(os.path.realpath(__file__))

style_provider = Gtk.CssProvider()
style_provider.load_from_path(os.path.join(script_dir,"style.css"))
Gtk.StyleContext.add_provider_for_screen(
    Gdk.Screen.get_default(),
    style_provider,
    Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    )

def walklevel(root_dir, min_depth, max_depth):
    # based on :
    # https://stackoverflow.com/a/234329/7175890
    # https://stackoverflow.com/a/28628704
    root_dir = root_dir.rstrip(os.path.sep)
    assert os.path.isdir(root_dir)
    root_depth = root_dir.rstrip(os.path.sep).count(os.path.sep)
    for root, dirs, files in os.walk(root_dir):
        local_depth = root.count(os.path.sep) - root_depth
        if min_depth <= local_depth <= max_depth:
            yield root, dirs, files
        elif local_depth > max_depth:
            del dirs[:]

def back_up_csv(root_dir, backed_csvs, table_name_noext):
    csv_abs = os.path.join(root_dir, table_name_noext + '.csv')
    if os.path.exists(csv_abs):
        table_backup = os.path.join(root_dir, table_name_noext + '.bak')
        os.rename(csv_abs, table_backup)
        backed_csvs += table_backup + '\n'
    return backed_csvs, csv_abs

def csv_to_dict(csv_abs):
    dict = {}
    with open(csv_abs,'r') as csvfile:
        tableReader = csv.reader(csvfile, delimiter='\t')
        for row in tableReader:
            if row :
                dict[row[0]] = []
                for duo in [row[n:n+2] for n in range(1, len(row), 2)]: # splits the row into duets of cells (visi and layer)
                    layer_data = {}
                    dict[row[0]].append(layer_data)
                    for idx, cell in enumerate(duo):
                        # -------------------------------------------------------------------------------------------------
                        # append visibility:
                        if idx == 0 : # if first item of duo
                            if '◡' in cell:
                                visibility = False
                            elif '👁' in cell:
                                visibility = True
                            else:
                                visibility = None
                            layer_data['visi'] = visibility

                        # -------------------------------------------------------------------------------------------------
                        # append layer and mask:
                        elif idx == 1 : # second item of duo
                            if '{' and '}' in cell :
                                layer_name = cell[:cell.find('{')]
                                mask_name = cell[cell.find('{')+1:cell.find('}')]
                                layer_data['layer'] = layer_name
                                layer_data['mask'] = mask_name
                            else :
                                layer_data['layer'] = cell
                                layer_data['mask'] = None
    return dict

def probe_xcf(xcf_abs):
    layers_list = []
    PROP_ACTIVE_LAYER = 2
    PROP_VISIBLE      = 8
    # open the file in readonly binary mode
    with open(xcf_abs, 'rb') as f:
        # go to the 30th bytes
        f.seek(30, 0)
        # read properties
        while True:
            prop_type = int.from_bytes(f.read(4), "big")
            prop_size = int.from_bytes(f.read(4), "big")
            f.read(prop_size)
            if prop_type == 0: #PROP_END
                break
        # read layers
        while True:
            next_layer_offset = int.from_bytes(f.read(8), "big")
            if not next_layer_offset: #end of layers offsets
                break;
            saved_pos = f.tell()
            f.seek(next_layer_offset + 12, 0)
            name_len = int.from_bytes(f.read(4), "big")
            layer_name0 = f.read(name_len).decode("utf-8")
            # remove terminating zero:
            layer_name = layer_name0.replace('\0', '')
            while True:
                prop_type = int.from_bytes(f.read(4), "big")
                prop_size = int(int.from_bytes(f.read(4), "big") / 4)
                for i in range(prop_size):
                    lastint = int.from_bytes(f.read(4), "big")
                if prop_type == PROP_VISIBLE:
                    visible = lastint
                elif prop_type == 0: #PROP_END
                    break
            # hierarchy pointer:
            hptr = int.from_bytes(f.read(8), "big")
            # mask pointer:
            mptr = int.from_bytes(f.read(8), "big")
            if mptr == 0:
                has_mask = 0
            else:
                has_mask = 1
            layer_data = {'visi':bool(visible), 'layer':layer_name, 'mask':has_mask}
            layers_list.insert(0, layer_data)
            f.seek(saved_pos, 0)
    return layers_list

def check_csvs_exist(orig_csv_abs, new_csv_abs, end_txt):
    nonexistent = []
    if not os.path.exists(orig_csv_abs):
        nonexistent.append(orig_csv_abs)
    if not os.path.exists(new_csv_abs):
        nonexistent.append(new_csv_abs)
    proceed = True
    if any(nonexistent):
        proceed = False
        end_txt += (_("The file:\n") +
                    nonexistent[0] +
                    _("\n\ndoes not exist.\n\n"
                    "Please chose a folder containing the files:\n"
                    "xcf_orig_table.csv\n"
                    "xcf_new_table.csv"))
        end_dialog = EndDialog('gtk-dialog-error', end_txt)
        end_dialog.run()
        print(proceed)
        exit()

# Verify that the orig_table.csv corresponds to the current state of the selected xcfs:
def check_valid_origcsv(root_dir, orig_csv_abs, end_txt):
    orig_dict = csv_to_dict(orig_csv_abs)
    current_dict = {}
    end_txt += _("xcf_orig_table contains at least the following error:\n\n")
    proceed = True
    for xcf_rel in sorted(orig_dict):
        xcf_abs = os.path.join(root_dir, xcf_rel)
        if not os.path.exists(xcf_abs):
            proceed = False
            end_txt += xcf_rel + _(" doesn't exist.")
            break
        # Create dictionary:
        current_dict[xcf_rel] = probe_xcf(xcf_abs)
    # Compare dictionaries:
    if proceed:
        for xcf in orig_dict:
            if proceed:
                if len(orig_dict[xcf]) != len(current_dict[xcf]):
                    proceed = False
                    end_txt += xcf + _(" contains ") + str(len(current_dict[xcf])) + _(" layers, not ") + str(len(orig_dict[xcf]))
                    break
                layer_id = 0
                for layer in orig_dict[xcf]:
                    if layer['layer'] != current_dict[xcf][layer_id]['layer']:
                        proceed = False
                        end_txt += _("The layer [") + layer['layer'] + _("] should be named [") + current_dict[xcf][layer_id]['layer'] + "]"
                        break
                    layer_id += 1
    # should not be an elif:
    if not proceed:
        end_txt += _("\n\nPlease make the necessary corrections or simply re-generate orig_table.")
        end_dialog = EndDialog('gtk-dialog-error', end_txt)
        end_dialog.run()
        print(proceed)
        exit()

# Check the validity of csvs before the "Modify Images" action :
def check_valid_mod_img(root_dir, orig_csv_abs, new_csv_abs):
    end_txt = _("Modification aborted:\n\n")
    check_csvs_exist(orig_csv_abs, new_csv_abs, end_txt)
    check_valid_origcsv(root_dir, orig_csv_abs, end_txt)

    # -------------------------------------------------------------------------------------
    # Check new:
    # Verify that the new_table.csv doesn't contain any error:
    warn_img_file_absent = _('Non-existing image files:\n\n')
    warn_img_file_absent_list = ''
    warn_mask_absent = _("Non-existing referred masks\n(the referred layer doesn't have a mask):\n\n")
    warn_mask_absent_list = ''
    warn_visi = _('Visibility (👁 or ◡) not indicated\nfor the following layers:\n\n')
    warn_visi_list = ''
    warn_mask_badname = _("Badly named masks:\n\n")
    warn_mask_badname_list = ''
    warn_mask_noxcf = _("The following masks don't exist because\nthe xcf itself doesn't exist:\n\n")
    warn_mask_noxcf_list = ''
    new_dict = csv_to_dict(new_csv_abs)
    # We re-create the current_dict, this time based on the xcfs present in the new table because having
    # an existing xcf in the new_table that was forgotten in the orig_table should be forgiven.
    current_dict = {}
    for xcf_rel in sorted(new_dict):
        xcf_abs = os.path.join(root_dir, xcf_rel)
        if os.path.exists(xcf_abs):
            current_xcf_exists = True
            current_dict[xcf_rel] = probe_xcf(xcf_abs)
        else:
            current_xcf_exists = False
        for new_layer_data in new_dict[xcf_rel]:
            new_layer_name = new_layer_data['layer']
            new_mask_name = new_layer_data['mask']
            new_layer_in_current = False
            new_mask_in_current = False
            if new_layer_name : # cell is not empty
                if current_xcf_exists :
                    for current_layer_data in current_dict[xcf_rel]:
                        current_layer_name = current_layer_data['layer']
                        if new_layer_name == current_layer_name:
                            new_layer_in_current = True
                        # -------------------------------------------------------------------------------------------------
                        # Absent specified mask :
                        if new_mask_name == current_layer_name:
                            new_mask_in_current = True
                            if not current_layer_data['mask']:
                                warn_mask_absent_list += '• ' + new_layer_name + '{' + current_layer_name + '} ------> ligne : ' + xcf_rel + '\n'
                # -------------------------------------------------------------------------------------------------
                # Absent image file :
                if not new_layer_in_current:
                    new_image_file = os.path.join(root_dir, os.path.dirname(xcf_rel), new_layer_name)
                    if not os.path.exists(new_image_file) :
                        warn_img_file_absent_list += '• ' + new_image_file + '\n'

                # -------------------------------------------------------------------------------------------------
                # Bad mask name :
                if new_mask_name is not None and not new_mask_in_current :
                    if current_xcf_exists:
                        warn_mask_badname_list += '• {' + new_mask_name + '} ------> ligne : ' + xcf_rel + '\n'
                    # -------------------------------------------------------------------------------------------------
                    # Absent mask because absent xcf :
                    else:
                        warn_mask_noxcf_list += '• {' + new_mask_name + '} ------> ' + xcf_rel + '\n'
                # -------------------------------------------------------------------------------------------------
                # Unindicated visibility :
                if new_layer_data['visi'] is None:
                    warn_visi_list += '• ' + new_layer_name + ' ------> ligne : ' + xcf_rel + '\n'

    # -------------------------------------------------------------------------------------------------
    # Warnings:
    w1_bool = bool(warn_img_file_absent_list)
    w2_bool = bool(warn_mask_absent_list)
    w3_bool = bool(warn_mask_badname_list)
    w4_bool = bool(warn_mask_noxcf_list)
    w5_bool = bool(warn_visi_list)
    w_bools = [w1_bool, w2_bool, w3_bool, w4_bool, w5_bool]

    w1_txt = warn_img_file_absent + warn_img_file_absent_list
    w2_txt = warn_mask_absent + warn_mask_absent_list
    w3_txt = warn_mask_badname + warn_mask_badname_list
    w4_txt = warn_mask_noxcf + warn_mask_noxcf_list
    w5_txt = warn_visi + warn_visi_list
    w_txts = [w1_txt, w2_txt, w3_txt, w4_txt, w5_txt]

    warnings_dict = {   'w1':[w1_bool,w1_txt],
                        'w2':[w2_bool,w2_txt],
                        'w3':[w3_bool,w3_txt],
                        'w4':[w4_bool,w4_txt],
                        'w5':[w5_bool,w5_txt]}
    proceed = True
    if any(w_bools):
        proceed = False
        avert = Warnings(warnings_dict)
        avert.run()
    print(proceed)

def check_valid_mod_names(root_dir, orig_csv_abs, new_csv_abs):
    end_txt = _("Renaming aborted :\n\n")
    check_csvs_exist(orig_csv_abs, new_csv_abs, end_txt)
    check_valid_origcsv(root_dir, orig_csv_abs, end_txt)

    # -------------------------------------------------------------------------------------------------
    # Check rename mode :
    orig_dict = csv_to_dict(orig_csv_abs)
    new_dict = csv_to_dict(new_csv_abs)
    proceed = True
    if len(new_dict) != len(orig_dict):
        proceed = False
        end_txt +=  (_("xcf_new_table contains ") + str(len(new_dict)) + _(" lines\n") +
                    _("while xcf_orig_table has ") + str(len(orig_dict))
                    )
        end_dialog = EndDialog('gtk-dialog-error', end_txt)
        end_dialog.run()
        print(proceed)
        exit()
    for xcf_rel in sorted(new_dict):
        if len(new_dict[xcf_rel]) != len(orig_dict[xcf_rel]):
            proceed = False
            end_txt +=  (_("According to xcf_new_table, ") + xcf_rel + _(" contains ") + str(len(new_dict[xcf_rel])) + _(" layers\n") +
                        _("while in reality it contains ") + str(len(orig_dict[xcf_rel]))
                        )
            end_dialog = EndDialog('gtk-dialog-error', end_txt)
            end_dialog.run()
            print(proceed)
            exit()
    print(proceed)


def generate_tables(root_dir, pattern, min_depth, max_depth):

    # find xcfs in subdirs:
    found_xcfs = []
    backed_csvs = ''
    for root, dirs, files in walklevel(root_dir, min_depth, max_depth):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                found_xcfs.append(os.path.join(root, name))

    # Create dictionary:
    table_dict = {}
    found_xcfs.sort()
    for xcf_abs in found_xcfs :
        xcf_rel = xcf_abs.split(root_dir + os.sep)[1]
        table_dict[xcf_rel] = probe_xcf(xcf_abs)

    # Create csv files:
    backed_csvs, orig_csv_abs = back_up_csv(root_dir, backed_csvs, 'xcf_orig_table')
    backed_csvs, new_csv_abs = back_up_csv(root_dir, backed_csvs, 'xcf_new_table')
    with open(orig_csv_abs, 'w') as csvfile:
        tableWriter = csv.writer(csvfile, delimiter='\t')
        for xcf in sorted(table_dict):
            row = [xcf]
            for layer_data in table_dict[xcf]:
                if layer_data['visi']:
                    row.append('👁')
                else:
                    row.append('◡')
                row.append(layer_data['layer'])
            tableWriter.writerow(row)
    copyfile(orig_csv_abs, new_csv_abs)
    end_txt = _('csv tables created in:\n') + root_dir
    if backed_csvs:
        end_txt += _("\n\n\ncsv files already existed.\nThe old ones were saved with the .bak extension.")
    end_dialog = EndDialog('gtk-dialog-info', end_txt)
    end_dialog.run()

class Warnings(Gtk.Dialog):
    def __init__(self, warnings_dict):
        dialog = Gtk.Dialog.__init__(self)
        area = self.get_content_area()
        self.set_title(_("TableGimper : Warnings"))
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        window_context = Gtk.Widget.get_style_context(self)
        Gtk.StyleContext.add_class(window_context, "warnings_background")
        top_label = Gtk.Label.new(_("Modification aborted.\nxcf_new_table contains these errors:"))
        top_label.set_justify(Gtk.Justification.CENTER)
        top_label.set_margin_top(30)
        top_label.set_margin_bottom(30)
        top_label.set_margin_start(15)
        top_label.set_margin_end(15)
        title_context = Gtk.Widget.get_style_context(top_label)
        Gtk.StyleContext.add_class(title_context, "warnings_title")
        area.add(top_label)
        box = Gtk.HBox()
        box.set_border_width(0)
        self.connect("destroy", Gtk.main_quit)
        for w in sorted(warnings_dict):
            if warnings_dict[w][0]:
                wmsg = warn_msg('gtk-dialog-warning', warnings_dict[w][1], "red_txt")
                box.pack_start(wmsg.msg_box, expand=False, fill=True, padding=0)
        area.add(box)
        self.show_all()

class warn_msg(Gtk.Box):
    def __init__(self, icon, text, color):
        self.msg_box = Gtk.EventBox()
        self.msg_box.set_border_width(10)
        box_context = Gtk.Widget.get_style_context(self.msg_box)
        Gtk.StyleContext.add_class(box_context, "warning_box")
        container = Gtk.VBox()
        container.set_border_width(20)
        msg_icon = Gtk.Image.new_from_icon_name(icon, 6)
        msg_label = Gtk.Label.new(text)
        msg_label.set_selectable(True)
        msg_label.set_margin_top(15)
        msg_label.set_margin_bottom(15)
        msg_context = Gtk.Widget.get_style_context(msg_label)
        Gtk.StyleContext.add_class(msg_context, color)
        container.pack_start(msg_icon, expand=False, fill=True, padding=0)
        container.pack_start(msg_label, expand=False, fill=True, padding=0)
        self.msg_box.add(container)

class EndDialog(Gtk.Dialog):
    def __init__(self, end_icon, end_txt):
        dialog = Gtk.Dialog.__init__(self)
        self.set_border_width(15)
        self.set_default_size(500, 200)
        area = self.get_content_area()
        self.set_title("TableGimper")
        self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        lbl1 = Gtk.Label.new("TableGimper")
        lbl1.set_margin_top(50)
        icon = Gtk.Image.new_from_icon_name(end_icon, Gtk.IconSize.DIALOG)
        icon.set_margin_top(50)
        lbl2 = Gtk.Label.new(end_txt)
        lbl2.set_margin_start(50)
        lbl2.set_margin_end(50)
        lbl2.set_margin_top(50)
        lbl2.set_margin_bottom(50)
        area.add(lbl1)
        area.add(icon)
        area.add(lbl2)
        self.connect("destroy", Gtk.main_quit)
        self.show_all()
