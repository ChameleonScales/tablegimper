#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import os, shutil, re

home = os.environ['HOME']
gimprc_path = os.path.join(home, ".config/GIMP/2.10/gimprc")
gimprc_bak_path = os.path.join(home, ".config/GIMP/2.10/gimprc_bak")

def override_gimp_prefs():
    gimp_threads = 3
    if os.path.exists(gimprc_path):
        os.rename(gimprc_path, gimprc_bak_path)
    gimprc = open(gimprc_path, "wt")
    has_proc = False
    # if gimprc already exists, copy its content and only replace num-proc
    if os.path.exists(gimprc_bak_path):
        gimprc_bak = open(gimprc_bak_path, "rt")
        for line in gimprc_bak:
            proc_found = re.match('(.*\(num-processors )(.*\))',line)
            if proc_found:
                has_proc = True
                proc_prefix = proc_found.groups()[0]
                proc_suffix = proc_found.groups()[1]
                repl_line = line.replace(   proc_prefix + proc_suffix,
                                            proc_prefix + str(gimp_threads) + ')')
                print(repl_line)
                gimprc.write(repl_line)
            else:
                gimprc.write(line)
        gimprc_bak.close()
    # if gimprc didn't exist or had no num-proc, then just append it
    if has_proc == False:
        gimprc.write('(num-processors 3)')
    gimprc.close()

def revert_gimp_prefs():
    if os.path.exists(gimprc_bak_path):
        os.remove(gimprc_path)
        os.rename(gimprc_bak_path, gimprc_path)
