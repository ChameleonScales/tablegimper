#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import os, gi, csv, gettext

#locale.setlocale(locale.LC_ALL, ('fr_FR', 'UTF-8'))
#domain = 'messages'
#locale.textdomain(domain)
#locale.bindtextdomain(domain, 'locale')
#locale.bind_textdomain_codeset(domain,'UTF-8')
t = gettext.translation(domain='messages', localedir='locale', fallback=True)
_ = t.gettext

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf
script_dir = os.path.dirname(os.path.realpath(__file__))
from help_dialog import HelpDialog, version

act_gen = _("Generate tables")
act_treat = _("Treat xcfs")
treat_l_m = _("Modify layers and masks")
treat_rename = _("Rename layers")
treat_export = _("Export to jpg")

class MainWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title = 'TableGimper')
        self.set_icon_from_file('images/icon.png')
        self.set_border_width(30)
        self.main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=15)
        main_context = Gtk.Widget.get_style_context(self.main_box)
        Gtk.StyleContext.add_class(main_context, "main_box")
        self.add(self.main_box)

        # Introduction
        intro_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        intro_label = Gtk.Label.new("TableGimper\n\nVersion " + str(version) + "\n\n")
        intro_logo = Gtk.Image.new_from_file(os.path.join(script_dir,"images/logo.png"))
        intro_box.pack_start(intro_label, expand=False, fill=True, padding=0)
        intro_box.pack_start(intro_logo, expand=False, fill=True, padding=0)
        self.main_box.pack_start(intro_box, expand=False, fill=True, padding=0)

        # Help
        help_btn = Gtk.Button.new_from_icon_name('gtk-help',6)
        help_btn.connect('clicked', self.on_help_btn_clicked)
        help_btn.set_label(_(" Help"))
        help_btn.set_halign(Gtk.Align.START)
        help_btn_inside = help_btn.get_child()
        help_btn_inside.set_border_width(10)
        help_btn_label=help_btn_inside.get_child().get_children()[1]
        help_btn_label.set_valign(Gtk.Align.CENTER)
        self.main_box.pack_start(help_btn, expand=False, fill=False, padding=50)


        # Directory choser
        dir_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=15)
        dir_columns = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=15)
        dir_label = Gtk.Label.new(_("Root folder:"))
        dir_label.set_xalign(0)
        self.dir_entry = Gtk.Entry()
        self.dir_entry.set_sensitive(False)
        self.dir_entry.set_activates_default(False)
        enforce_target = Gtk.TargetEntry.new('text/plain', Gtk.TargetFlags(4), 129)
        self.main_box.drag_dest_set(Gtk.DestDefaults.ALL, [enforce_target], Gdk.DragAction.COPY)
        self.main_box.connect("drag-motion", self.on_drag_motion)
        self.main_box.connect("drag-leave", self.on_drag_leave)
        self.main_box.connect("drag-data-received", self.on_drag_data_received)
        dir_btn = Gtk.Button.new_from_icon_name('folder',3)
        dir_btn.set_label(_(' Browse...'))
        dir_btn.connect('clicked', self.on_dir_btn_clicked)
        dir_box.pack_start(dir_label, expand=False, fill=True, padding=0)
        dir_box.pack_start(dir_columns, expand=False, fill=True, padding=0)
        dir_columns.pack_start(self.dir_entry, expand=True, fill=True, padding=0)
        dir_columns.pack_start(dir_btn, expand=False, fill=True, padding=0)
        self.main_box.pack_start(dir_box, expand=False, fill=True, padding=0)

        # Action dropdown
        self.action_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        action_label = Gtk.Label.new("Action :")
        action_store = Gtk.ListStore(str)
        action_store.append([act_gen])
        action_store.append([act_treat])
        action_dropdown = Gtk.ComboBox.new_with_model(action_store)
        action_dropdown.connect("changed", self.on_action_dropdown_changed)
        renderer_text = Gtk.CellRendererText()
        action_dropdown.pack_start(renderer_text, True)
        action_dropdown.add_attribute(renderer_text, "text", 0)
        self.action_box.pack_start(action_label, expand=False, fill=True, padding=0)
        self.action_box.pack_start(action_dropdown, expand=False, fill=True, padding=0)
        self.main_box.pack_start(self.action_box, expand=False, fill=True, padding=0)

        # Table generator (Tabler)
        self.tabler_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing = 10)
        depth_label = Gtk.Label.new(_("Searching depths (0 = root) :"))
        depth_label.set_xalign(0)
        tabler_columns_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing = 10)
        tabler_labels_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing = 10)
        tabler_spins_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing = 10)
        tabler_run_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        min_label = Gtk.Label.new("Min :")
        max_label = Gtk.Label.new("Max :")
        min_label.set_halign(Gtk.Align.START)
        max_label.set_halign(Gtk.Align.START)
        minSpinAdj = Gtk.Adjustment(value = 1, lower = 0, upper = 10, step_increment = 1)
        maxSpinAdj = Gtk.Adjustment(value = 1, lower = 0, upper = 10, step_increment = 1)
        self.min_spin = Gtk.SpinButton.new(minSpinAdj,1,0)
        self.min_spin.set_numeric(True)
        self.max_spin = Gtk.SpinButton.new(maxSpinAdj,1,0)
        self.max_spin.set_numeric(True)
        tabler_run_btn = Gtk.Button.new_from_icon_name("spreadsheet",6)
        tabler_run_btn.connect('clicked', self.on_tabler_run_btn_clicked)
        tabler_run_btn.set_label(_(" Run"))
        tabler_run_btn_inside = tabler_run_btn.get_child()
        tabler_run_btn_inside.set_border_width(10)
        tabler_run_btn_label = tabler_run_btn.get_child().get_child().get_children()[1]
        tabler_run_btn_label.set_valign(Gtk.Align.CENTER)
        tabler_run_box.set_valign(Gtk.Align.END)
        self.tabler_box.pack_start(depth_label, expand=False, fill=True, padding=15)
        self.tabler_box.pack_start(tabler_columns_box, expand=False, fill=True, padding=0)
        self.tabler_box.pack_start(tabler_run_box, expand=True, fill=True, padding=0)
        tabler_columns_box.pack_start(tabler_labels_box, expand=False, fill=True, padding=0)
        tabler_columns_box.pack_start(tabler_spins_box, expand=False, fill=True, padding=0)
        tabler_labels_box.pack_start(min_label, expand=True, fill=True, padding=0)
        tabler_labels_box.pack_start(max_label, expand=True, fill=True, padding=0)
        tabler_spins_box.pack_start(self.min_spin, expand=True, fill=True, padding=0)
        tabler_spins_box.pack_start(self.max_spin, expand=True, fill=True, padding=0)
        tabler_run_box.pack_end(tabler_run_btn, expand=False, fill=True, padding=0)
        self.main_box.pack_start(self.tabler_box, expand=True, fill=True, padding=0)

        # XCF updater (Gimper)
        self.gimper_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.modify_images_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        self.export_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        modimg_checkboxes_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        modimg_labels_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        gimper_run_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)

        treatment_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=20)
        treatment_label = Gtk.Label.new(_("Treatment type :"))
        treatment_store = Gtk.ListStore(str)
        treatment_store.append([treat_l_m])
        treatment_store.append([treat_rename])
        treatment_store.append([treat_export])
        self.treatment_dropdown = Gtk.ComboBox.new_with_model(treatment_store)
        self.treatment_dropdown.set_active(0)
        self.modif_type = treat_l_m
        self.treatment_dropdown.connect("changed", self.on_treatment_dropdown_changed)
        renderer_text = Gtk.CellRendererText()
        self.treatment_dropdown.pack_start(renderer_text, True)
        self.treatment_dropdown.add_attribute(renderer_text, "text", 0)
        treatment_box.pack_start(treatment_label, expand=False, fill=True, padding=0)
        treatment_box.pack_start(self.treatment_dropdown, expand=False, fill=True, padding=0)
        self.gimper_box.pack_start(treatment_box, expand=False, fill=True, padding=0)

        self.make_masks_check = Gtk.CheckButton()
        make_masks_label = Gtk.Label.new(_("Add mask if first layer doesn't have one (extracted blue channel)"))
        make_masks_label.set_halign(Gtk.Align.START)
        export_qual_label = Gtk.Label.new(_("Quality (%) :"))
        export_qual_label.set_halign(Gtk.Align.START)
        export_qual_SpinAdj = Gtk.Adjustment(value = 95, lower = 0, upper = 100, step_increment = 5)
        self.export_qual_spin = Gtk.SpinButton.new(export_qual_SpinAdj,1,0)
        self.export_qual_spin.set_numeric(True)
        gimper_run_btn = Gtk.Button.new_from_icon_name("gimp",6)
        gimper_run_btn.connect('clicked', self.on_gimper_run_btn_clicked)
        gimper_run_btn.set_label(_(" Run"))
        gimper_run_btn_inside = gimper_run_btn.get_child()
        gimper_run_btn_inside.set_border_width(10)
        gimper_run_btn_label = gimper_run_btn.get_child().get_child().get_children()[1]
        gimper_run_btn_label.set_valign(Gtk.Align.CENTER)
        gimper_run_box.set_valign(Gtk.Align.END)
        self.gimper_box.pack_start(self.modify_images_box, expand=False, fill=True, padding=10)
        self.gimper_box.pack_start(self.export_box, expand=False, fill=True, padding=10)
        self.gimper_box.pack_start(gimper_run_box, expand=True, fill=True, padding=0)
        self.modify_images_box.pack_start(modimg_checkboxes_box, expand=False, fill=True, padding=0)
        self.modify_images_box.pack_start(modimg_labels_box, expand=False, fill=True, padding=0)
        self.export_box.pack_start(export_qual_label, expand=False, fill=True, padding=0)
        self.export_box.pack_start(self.export_qual_spin, expand=False, fill=True, padding=0)
        modimg_checkboxes_box.pack_start(self.make_masks_check, expand=True, fill=True, padding=0)
        modimg_labels_box.pack_start(make_masks_label, expand=True, fill=True, padding=0)
        gimper_run_box.pack_end(gimper_run_btn, expand=False, fill=True, padding=0)
        self.main_box.pack_start(self.gimper_box, expand=True, fill=True, padding=0)

    def on_help_btn_clicked(self, widget):
        help_dialog = HelpDialog(self)
        help_dialog.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        help_dialog.run()
        help_dialog.destroy()

    def on_drag_motion(self, widget, drag_context, x,y, data):
        self.drag_context = Gtk.Widget.get_style_context(self.main_box)
        Gtk.StyleContext.add_class(self.drag_context, "drag_item")

    def on_drag_leave(self, widget, drag_context, time):
        Gtk.StyleContext.remove_class(self.drag_context, "drag_item")

    def on_drag_data_received(self, widget, drag_context, x,y, data,info, time):
        dragged = data.get_text()
        path0 = dragged.replace('file://', '')
        path = path0.replace('\n', '')
        if os.path.isfile(path):
            path = os.path.dirname(path)
        self.dir_entry.set_text(path)
        self.action_box.show()

    def on_dir_btn_clicked(self, widget):
        dir_dialog = Gtk.FileChooserDialog( title = _('TableGimper : Root folder'),
                                        action = Gtk.FileChooserAction.SELECT_FOLDER )
        dir_dialog.add_buttons( 'Cancel', Gtk.ResponseType.CANCEL,
                                'Open', Gtk.ResponseType.OK)
        response = dir_dialog.run()
        if response == Gtk.ResponseType.OK :
            self.dir_entry.set_text(dir_dialog.get_filename())
            self.action_box.show()
        dir_dialog.destroy()

    def on_action_dropdown_changed(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            action = model[tree_iter][0]
            if action == act_gen :
                self.tabler_box.show()
                self.gimper_box.hide()
            elif action == act_treat :
                self.tabler_box.hide()
                self.gimper_box.show()

    def on_treatment_dropdown_changed(self, combo):
        tree_iter = combo.get_active_iter()
        if tree_iter is not None:
            model = combo.get_model()
            self.modif_type = model[tree_iter][0]
            if self.modif_type == treat_l_m :
                self.export_box.hide()
                self.modify_images_box.show()
            elif self.modif_type == treat_rename :
                self.modify_images_box.hide()
                self.export_box.hide()
            elif self.modif_type == treat_export :
                self.modify_images_box.hide()
                self.export_box.show()

    def on_tabler_run_btn_clicked(self, widget):
        print(self.dir_entry.get_text())
        print("tabler")
        print(int(self.min_spin.get_value()))
        print(int(self.max_spin.get_value()))
        Gtk.main_quit()

    def on_gimper_run_btn_clicked(self, widget):
        print(self.dir_entry.get_text())
        print("gimper")
        if self.modif_type == treat_l_m :
            print("treat_l_m")
            print(self.make_masks_check.get_active())
        elif self.modif_type == treat_rename :
            print("treat_rename")
        elif self.modif_type == treat_export :
            print("treat_export")
            print(self.export_qual_spin.get_value())
        Gtk.main_quit()

    def initial_show(self):
        window.show_all()
        self.action_box.hide()
        self.tabler_box.hide()
        self.gimper_box.hide()
        self.export_box.hide()
        window.resize(800,1)

window = MainWindow()
style_provider = Gtk.CssProvider()
style_provider.load_from_path(os.path.join(script_dir, "style.css"))
Gtk.StyleContext.add_provider_for_screen(
    Gdk.Screen.get_default(),
    style_provider,
    Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
)
window.connect("delete-event", Gtk.main_quit)
window.initial_show()
Gtk.main()
