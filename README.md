# tablegimper

A tool for batch processing of Gimp xcf files using spreadsheets.

<img src="/images/tablegimper-screenshot.png"  width="40%" height="40%">

## What kind of batch processing?

Generate new xcfs or modify the contents of existing xcfs without opening Gimp:

- import files as layers
- remove, move and copy existing layers and masks
- generate masks from layers
- rename layers
- export xcfs to jpgs
- ... something you want me to implement?

⚠ Layer groups are not supported (I may work on it when their memory inefficiency gets addressed in Gimp's core development. See [this thread](https://www.gimp-forum.net/Thread-Less-data-heavier-file) for more info.)

## OS and dependencies

As of now, only Linux is supported.

I have no real plan for Windows or Mac support. Help and pull requests for multiplatform support are always welcome, just know that I don't have a lot of time to spend on it.

### For Debian 11:
- gimp_2.10.8 (or above but in the 2.10.x series)
- python-is-python2
- gimp-python_2.10.8
- libffi6_3.2.1
- python-cairo_1.16.2
- python-gobject-2_2.28.6
- python-gtk2_2.24.0-5.1
- python-numpy_1.16.2
- python-pkg-resources_40.8.0

Apart from python-is-python2, all these packages are available on https://packages.debian.org as Debian Buster .deb packages.

⚠ Warning: if you use GIMP from your system's repository, you have to replace `flatpak run org.gimp.GIMP` by `gimp` in the script named "tableGimper".

### For Debian 12:
- You have to use GIMP's flatpak (`flatpak install org.gimp.GIMP`) and remove the Debian repository version (`sudo apt remove GIMP`) because GIMP's dependencies for plugins are too dated and conflict with Debian 12's apt packages.
- If it doesn't work out of the box, you may have to install some dependencies listed for Debian 11 (I didn't test from scratch but it works on my Debian 12 upgraded from Debian 11)

## Installation
Download and extract the contents anywhere you want and run the file named "tableGimper". No need to put it in Gimp's plug-in directory. This is a third-party tool that doesn't require Gimp to be open. Just make sure that tableGimper and all the .py files are executable.

## Usage
### All the instructions are layed out in the program's Help dialog. To make it simple here:

You start with a table that has this structure, where the first column has the relative paths of your xcfs and the following columns have the names of their layers along with each layer's visibility:

| xcf path | | layer 1 | | layer 2 | | layer 3 | | layer 4 |
| - | - | - | - | - | - | - | - | - |
| A/A.xcf | 👁 | A0.jpg | 👁 | A1.png | ◡ | A2.png | ◡ | A3.png |
| B/B.xcf | 👁 | B0.jpg | 👁 | B1.png | ◡ | B2.png | | |
| C/C.xcf | 👁 | C0.jpg | ◡ | C1.png | 👁 | C2.png | | |
	
You then apply modifications to this table by following some rules (see the program's help dialog for details).
Here's an example:

| xcf path |  | layer 1 |  | layer 2 |  | layer 3 |
| - | - | - | - | - | - | - |
| A/A.xcf | 👁 | A0 | 👁 | A1{A0} | ◡ | A2.png |
| B/B.xcf | ◡ | B0 |  |  | 👁 | B2.png{B1} |
| C/C.xcf | ◡ | C0.jpg | 👁 | C1.jpg |  |  |
| D/D.xcf | 👁 | D0.jpg | 👁 | D1.png | ◡ | D2.png |

You can then use this modified table to apply the desired modifications to your xcfs as a batch process called "Treatment" in TableGimper.

## Support
You can email me at chameleonscales@protonmail.com

## Roadmap
For my own use this is as good as I need it to be at the moment. If you need some new features or improvements, feel free to email me.

## Contributing
You can easily translate it in your language using Poedit. The default is english and I added a french version. You can follow the model of the french one to add your own language (in the locale folder) free to ask for more detailed instructions.

Other than that I'm open to all kinds of contributions I guess.

## Credits

**Author:** Caetano Veyssières

**With help from:** tmanni, Akkana Peck (akk), Ofnuts, eepjr24

**License:** GPL v3

